package com.matritel.MultipleThreading;

import com.matritel.MultipleThreading.RunnableDemo;

public class TestOfThreads {

    public static void main(String[] args) {

        RunnableDemo runnableDemo1 = new RunnableDemo("Thread -1");
        RunnableDemo runnableDemo2 = new RunnableDemo("Thread-2");

        runnableDemo1.start();
        runnableDemo2.start();
    }
}
