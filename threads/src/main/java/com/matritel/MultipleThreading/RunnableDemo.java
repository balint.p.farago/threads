package com.matritel.MultipleThreading;

public class RunnableDemo implements Runnable{

    private Thread thread;
    private String threadName;

    public RunnableDemo(String threadName) {
        this.threadName = threadName;
        System.out.println("Creating a " + threadName);
    }

    public void run() {

        for (int i = 1; i <=10; i++) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println(e);
            }
        }
    }

    public void start (){
        System.out.println("Starting a " + threadName);
        if (thread == null) {
            thread = new Thread(this, threadName);
            thread.start();
        }
    }
}
