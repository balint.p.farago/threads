package com.matritel.multithread;

public class App {
    public static void main(String[] args) {

        Thread t1 = new Thread(new Counter());
        Thread t2 = new Thread(new Counter());
        t1.start();  // thread0
        t2.start();  // thread1
        System.out.println("Finished");  // Main thread
        //These 3 threads are running parallel and the order is decided by the OS and JVM
    }
}
