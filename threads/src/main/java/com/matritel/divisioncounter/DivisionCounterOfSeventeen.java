package com.matritel.divisioncounter;

public class DivisionCounterOfSeventeen implements Runnable{

    private long startNumber;
    private long endNumber;


    public DivisionCounterOfSeventeen(long startNumber, long endNumber) {
        this.startNumber = startNumber * 1000000000L + 1;
        this.endNumber = endNumber * 1000000000L;
    }

    @Override
    public void run() {
        countDivisionsOfSeventeenFromTo(startNumber, endNumber);
    }

    public void countDivisionsOfSeventeenFromTo(long startNumber, long endNumber){

        long startTime = System.currentTimeMillis();

        int cnt = 0;
        for (long i = startNumber; i < endNumber; i++){
            if (i % 17 == 0){
                cnt++;
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println(cnt);
        System.out.println(endTime - startTime);
        System.out.println();


    }
}
